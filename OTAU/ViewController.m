//
//  ViewController.m
//  OTAU
//
/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2014
 *
 *  This software is provided to the customer for evaluation
 *  purposes only and, as such early feedback on performance and operation
 *  is anticipated. The software source code is subject to change and
 *  not intended for production. Use of developmental release software is
 *  at the user's own risk. This software is provided "as is," and CSR
 *  cautions users to determine for themselves the suitability of using the
 *  beta release version of this software. CSR makes no warranty or
 *  representation whatsoever of merchantability or fitness of the product
 *  for any particular purpose or use. In no event shall CSR be liable for
 *  any consequential, incidental or special damages whatsoever arising out
 *  of the use of or inability to use this software, even if the user has
 *  advised CSR of the possibility of such damages.
 *
 ******************************************************************************/
//

#import "ViewController.h"
#import "AppDelegate.h"

#import "ApplicationImage.h"

@interface ViewController ()
@property BOOL isUpdateRunning;
@property BOOL isInitRunning;
@property BOOL isAbortButton;
@property BOOL peripheralConnected;
@property uint8_t expectedCsKey;
@end

@implementation ViewController

@synthesize firmwareName, targetName, updateButtonName;
@synthesize firmwareFilename, targetPeripheral;
@synthesize isAbortButton, isUpdateRunning, isInitRunning, peripheralConnected, expectedCsKey;

@synthesize deviceAddress, connectionState, crystalTrim, fwVersion;
@synthesize statusLog;
@synthesize startButton, abortButton, progressBar, percentLabel;
@synthesize modeLabel, challengeLabel, appVersionLabel, appVersion;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (@available(iOS 13.0, *)) {
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
	// Do any additional setup after loading the view, typically from a nib.
    [[OTAU sharedInstance] setOTAUDelegate:self];
    
    [self statusMessage: @"Start: Load CS key JSON\n"];
    if ([[OTAU sharedInstance] parseCsKeyJson:@"cskey_db"]) {
        [self statusMessage: @"Success: Load CS key JSON\n"];
    }
    else {
        [self statusMessage: @"Fail: Load CS key JSON\n"];
    }

    peripheralConnected = NO;
    [connectionState setText:@"DISCONNECTED"];
    
    progressBar.progress = 0.0;
    
    isInitRunning = NO;
    
    // Did we open App with email or dropbox attachment?
    AppDelegate *appDelegate= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.urlImageFile)
        [self handleOpenURL:appDelegate.urlImageFile];
}

- (void)viewDidAppear:(BOOL)animated {
    AppDelegate *appDelegate= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!peripheralConnected) {
        [modeLabel setText:@"-"];
    }
    else if ([appDelegate.peripheralInBoot boolValue]==YES) {
        [modeLabel setText:@"BOOT"];
    }
    else {
        [modeLabel setText:@"APP"];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"chooseFirmware"]) {
        FirmwareSelector *fs = (FirmwareSelector *)[segue destinationViewController];
        fs.firmwareDelegate = self;
    }

    if ([[segue identifier] isEqualToString:@"chooseTarget"]) {
        DiscoverViewController *dvc = (DiscoverViewController *)[segue destinationViewController];
        dvc.discoveryViewDelegate = self;
    }
}



// Delegates
-(void) firmwareSelector:(NSString *) filepath {
    if ([filepath isEqualToString:@""]) {
    }
    else {
        NSString *filename = [[filepath lastPathComponent] stringByDeletingPathExtension];
        [firmwareName setTitle:filename forState:UIControlStateNormal];
        [firmwareName setAlpha:1.0];
        firmwareFilename = filepath;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    [self setStartAndAbortButtonLook];
    
    self.startButton.enabled = YES;
}

-(void) setTarget:(id)peripheral
{
    if (peripheral != nil)
    {
        [targetName setTitle:[peripheral name] forState:UIControlStateNormal];
        [targetName setAlpha:1.0];
        targetPeripheral = peripheral;
        [connectionState setText:@"CONNECTED"];
        isInitRunning = YES;
        [self setStartAndAbortButtonLook];
        [[OTAU sharedInstance] initOTAU:peripheral];
    }
    
    // Dismiss Discovery View and any nulify all delegates set up for it.
    [[Discovery sharedInstance] setDiscoveryDelegate:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
    [[Discovery sharedInstance] stopScanning];
    [self setStartAndAbortButtonLook];
}

- (IBAction)cancelFirmware:(id)sender
{
    [firmwareName setTitle:@"Set Firmware" forState:UIControlStateNormal];
    [firmwareName setAlpha:0.4];
    firmwareFilename=nil;
    [self setStartAndAbortButtonLook];
}

- (IBAction)cancelTarget:(id)sender
{
    [self clearTarget];
}

//============================================================================
// Called when the Start button is pressed
// 
- (IBAction)startUpdate:(id)sender
{
    if (isAbortButton==NO)
    {
        if (firmwareFilename!=nil) {
//            NSMutableData *binaryFirmware = [[ApplicationImage sharedInstance] convertFirmwareToBinary:firmwareFilename];
        }
        
        if (firmwareFilename!=nil && targetPeripheral!=nil)
        {
            isAbortButton=YES;
            isUpdateRunning=YES;
            [self statusMessage:@"------[ Update Started ]------\n"];
            [[OTAU sharedInstance] startOTAU:firmwareFilename];
            self.progressBar.progress = 0.0;
            [percentLabel setText: @"0%"];
            [self setStartAndAbortButtonLook];
        }
    }
}

//============================================================================
// Called when the Abort button is pressed
//
- (IBAction)abortUpdate:(id)sender
{
    if (isAbortButton==YES)
    {
        isAbortButton=NO;
        [self statusMessage:@"Update Aborted\n"];
        [[OTAU sharedInstance]  abortOTAU:targetPeripheral];
        isUpdateRunning=NO;
        [self setStartAndAbortButtonLook];
    }
}

-(void) clearTarget {
    [targetName setTitle:@"set target" forState:UIControlStateNormal];
    [targetName setAlpha:1.0];
    targetPeripheral=nil;
    [self setStartAndAbortButtonLook];
}

//============================================================================
// Set the look of the Start and Abort Buttons to reflect whether or not the button is available.
//
// Call this method after...
//  - Setting the App File
//  - Choosing the target
//  - After Start Button is Pressed
//  - After Abort button is pressed
//  - At the end of an Update
//  - Update error received.
//
// To Disable button: set Alpha to 0.2 and Enabled to NO
// To Enable Button:  set Alpha to 1.0 and Enabled to YES
//
// Enable Start Button if App File and Target are both valid and update not running.
// Enable Abort Button if Update is running.
-(void) setStartAndAbortButtonLook
{
    if (isInitRunning || isUpdateRunning) {
        [startButton setEnabled:NO];
        [firmwareName setEnabled:NO];
        [targetName setEnabled:NO];
    }
    else {
        [firmwareName setEnabled:YES];
        [targetName setEnabled:YES];
        if (targetPeripheral!=nil && firmwareFilename!=nil)
        {
            [startButton setEnabled:YES];
        }
        else
        {
            [startButton setEnabled:NO];
        }
    }
    
    if (isUpdateRunning)
    {
        [abortButton setHidden:NO];
        [progressBar setHidden:NO];
        [percentLabel setHidden: NO];
        [abortButton setEnabled:YES];
        [startButton setHidden:YES];
    }
    else
    {
        [abortButton setHidden:YES];
        [abortButton setEnabled:NO];
        [progressBar setHidden:YES];
        [percentLabel setHidden: YES];
        [startButton setHidden:NO];
    }
}


/****************************************************************************/
/*								Open With.....                              */
/****************************************************************************/
-(void) handleOpenURL:(NSURL *)url {
    //NSError *outError;
    //NSString *fileString = [NSString stringWithContentsOfURL:url
     //                                               encoding:NSUTF8StringEncoding
       //                                                error:&outError];
    NSString *filename = [[url lastPathComponent] stringByDeletingPathExtension];
    [firmwareName setTitle:filename forState:UIControlStateNormal];
    [firmwareName setAlpha:1.0];
    firmwareFilename = url.path;
    [self statusMessage:[NSString stringWithFormat:@"Imported File %@\n",firmwareFilename]];
}



/****************************************************************************/
/*								Delegates                                   */
/****************************************************************************/
//============================================================================
// Update the progress bar when a progress percentage update is received during OTAU update.
-(void) didUpdateProgress: (uint8_t) percent {
    self.progressBar.progress = percent / 100.0f;
    [percentLabel setText: [NSString stringWithFormat:@"%d%%", percent]];
    if (percent == 100) {
        // Transfer is complete so update controls to hide abort and progress bar,
        // but show a disabled start button as init is running again to query versions
        // and cs keys. We will receive the "complete" delegate when that is done.
        isUpdateRunning = NO;
        isInitRunning = YES;
        [self setStartAndAbortButtonLook];
    }
}

//============================================================================
//
-(void) didUpdateBtaAndTrim:(NSData *)btMacAddress :(NSData *)crystalTrimValue {
    [self.deviceAddress setText: @"-"];
    [self.crystalTrim setText: @"-"];
    
    if (btMacAddress != nil && crystalTrimValue != nil) {
        // Display bluetooth address.
        const uint8_t *octets = (uint8_t*)[btMacAddress bytes];
        NSString *display = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                             octets[0], octets[1], octets[2], octets[3], octets[4], octets[5]];
        
        [deviceAddress setText:display];
        
        // Display crystal trim.
        const uint16_t *trim = (uint16_t*)[crystalTrimValue bytes];
        display = [NSString stringWithFormat:@"0x%X", *trim];
        [crystalTrim setText:display];
        [self statusMessage: @"Success: Read CS keys.\n"];
    }
    else {
        [self statusMessage: @"Failed to read CS keys.\n"];
    }
    
    if (isInitRunning) {
        isInitRunning = NO;
        [self setStartAndAbortButtonLook];
    }
}

//============================================================================
// This delegate is called after we have called initOTAU and the library has finished
// querying the peripheral.
-(void) didUpdateVersion:(uint8_t)otauVersion {
    [self.fwVersion setText: @"-"];
    
    if (otauVersion > 3) {
        [fwVersion setText: [NSString stringWithFormat:@"%d", otauVersion]];
        [self statusMessage: @"Success: Get version.\n"];
    }
    else {
        [self statusMessage: @"Failed to read OTAU version.\n"];
    }
}

//============================================================================
//
-(void) didUpdateAppVersion:(NSString*)appVersionString {
    [self statusMessage: [NSString stringWithFormat:@"Success: Got app version:%@\n", appVersionString]];
    if (appVersionString != nil) {
        [appVersion setText: appVersionString];
        [appVersionLabel setHidden: NO];
        [appVersion setHidden: NO];
    }
    else {
        [appVersionLabel setHidden: YES];
        [appVersion setHidden: YES];
    }
}

//============================================================================
// This delegate is called when the selected peripheral connection state changes.
-(void) didChangeConnectionState:(bool)isConnected {
    [deviceAddress setText: @"-"];
    [crystalTrim setText: @"-"];
    [fwVersion setText: @"-"];
    [modeLabel setText: @"-"];
    [challengeLabel setText: @"-"];
    [appVersion setText: @"-"];
    if (isConnected) {
        peripheralConnected = YES;
        [connectionState setText:@"CONNECTED"];
    }
    else {
        peripheralConnected = NO;
        [connectionState setText:@"DISCONNECTED"];
    }
}

//============================================================================
//
-(void) didChangeMode: (bool) isBootMode {
    if (isBootMode) {
        [modeLabel setText: @"BOOT"];
    }
    else {
        [modeLabel setText: @"APP"];
    }
}

//============================================================================
//
-(void) didUpdateChallengeResponse:(bool)challengeEnabled {
    if (challengeEnabled) {
        [challengeLabel setText: @"ENABLED"];
    }
    else {
        [challengeLabel setText: @"DISABLED"];
    }
}

//============================================================================
// Display an Alert upon successful completion
//
-(void) completed:(NSString *) message {
    isAbortButton=NO;
    isUpdateRunning=NO;
    [self setStartAndAbortButtonLook];
    NSString *title;
    
    title = @"OTAU";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

//============================================================================
// Display the status in the Text view
-(void) statusMessage:(NSString *)message
{
    [statusLog setText:[statusLog.text stringByAppendingString:message]];
    NSRange range = NSMakeRange(statusLog.text.length - 1, 1);
    [statusLog scrollRangeToVisible:range];
 }


//============================================================================
// Display error as an Alert
-(void) otauError:(NSError *) error {
    
    if (isInitRunning) {
        isInitRunning = NO;
        [self clearTarget];
    }
    else if (isUpdateRunning) {
        isAbortButton = NO;
        isUpdateRunning = NO;
    }
    
    // Convert error code to 4 character string, as error codes will be in the range 1000-9999
    NSString *errorCodeString = [NSString stringWithFormat:@"%4d",(int)error.code];
    
    // Lookup Error string from error code
    NSString *errorString = NSLocalizedStringFromTable (errorCodeString, @"ErrorCodes", nil);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTAU Error"
                                                    message:errorString
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    [self setStartAndAbortButtonLook];
}


@end
